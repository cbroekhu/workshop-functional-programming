package com.cap.fp.dia_anonymous_inner_class;

@FunctionalInterface
interface Turnable {
    int turn(int degrees, int b);
}

public class ClassesAnonymous {

    public static void main( String args[]){

        Turnable turnable = new Turnable(){

            @Override
            public int turn(int degrees, int b) {
                return degrees * b;
            }

        };

        int turned = turnable.turn( 45, 8);

        System.out.println( turned);

    }
}
