package com.cap.fp.case_min1;

public class Screen {
	
	public static void main( String[] args){
		
		Button button = new Button();
	
		button.addClickListener( new ClickListener(){

			@Override
			public void click() {
				System.out.println("You clicked");
				
			}
			
		});
		
		button.addMoveListener( new MoveListener(){
			
			@Override
			public void move( int x, int y) {
				System.out.println("You moved to: " + x + "," + y);
				
			}
		
		});
		
		button.click();
		button.move( 234, 678);
		
	}

}
