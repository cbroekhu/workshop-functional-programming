package com.cap.fp.case_min1;

public interface MoveListener {
	
	public void move( int x, int y);

}
