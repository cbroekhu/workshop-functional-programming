package com.cap.fp.dia_lambdas;

@FunctionalInterface
interface Turnable {
    int turn(int degrees, int b);
}

public class Lambdas1 {

    private static int getSomeInt() {
        return 8;
    }
    public static void main( String args[]){

        int trouble = 4;

        Turnable  turnable1 = ( int a, int b) -> { return (a + b);};
        Turnable  turnable2 = (  a,  b) -> { return (a + b);};
        Turnable  turnable3 = (  x,  y) -> x + y;
        Turnable  turnable4 = (  x,  y) -> x + 8;
        Turnable  turnable5 = (  a,  b) ->  { return (a + b);};
        Turnable  turnable6 = (  a,  b) -> { int j = 3; return j * (a + b);};

        Turnable  turnable7 = (  a,  b) -> {
            int j = 3;
            int k = getSomeInt();
            return j * a + k * b;
        };

        Turnable  turnable8 = (  a,  b) -> {
            int j = 3;
            int k = getSomeInt();
//            trouble = 9;
            return j * a + k * b;
        };

    }

}
