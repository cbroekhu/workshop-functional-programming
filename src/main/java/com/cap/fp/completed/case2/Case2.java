package com.cap.fp.completed.case2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class Case2 {

    private static final String STRINGTOFIND = "34567899999999876543";

    public static void main( String[] args) throws IOException {

        List<Path> paths = Files.list( Paths.get("D:/Data"))
                .filter( (f) -> !f.toFile().isDirectory())
                .filter( (f) -> f.toFile().getName().endsWith(".txt"))
                .collect( Collectors.toList());

        processFiles( paths);

    }

    private static void processFiles( List<Path> paths) throws IOException {

        List<Future> futures = new ArrayList<>();

        int cpus = Runtime.getRuntime().availableProcessors();
        System.out.println(cpus);

        ExecutorService service = Executors.newFixedThreadPool(cpus);

        for( Path path: paths){

            Future<String> future = service.submit(

                    () -> processFile( path)
            );

            futures.add( future);

        }

         for( Future<String> future: futures){

            try {

                String file = future.get();
                if( !file.isEmpty()){
                    System.out.println("File: " + file);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }

        service.shutdown();

    }

    private static String processFile( Path path) throws IOException {

        String file = "";

        Optional<String> optional = Optional.of( Files.lines( path)
                .filter( (l) -> l.contains( STRINGTOFIND))
                .peek(System.out::println)
                .findFirst()
                .orElse(""));

        if ( !optional.get().isEmpty()) {
            file = path.toFile().getName();
        }

        return file;
    }

}
