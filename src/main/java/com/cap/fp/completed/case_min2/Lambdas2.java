package com.cap.fp.completed.case_min2;

@FunctionalInterface
interface Turnable {
    void turn(int degrees, int b);
}

public class Lambdas2 {

    private static void useTurnable(Turnable turnable, int degrees, int b){

        turnable.turn( degrees, b);

    }

    private static void printBothValues( int k, int e){
        System.out.println( "Degrees: " + k + ", b: " + e) ;
    }
    
    public static void main(String args[]) {

        Lambdas2.useTurnable( (a, b) -> {} , 79, 9);
        Lambdas2.useTurnable( Lambdas2::printBothValues , 360, 2);
        
//        System.out::println( turned);

    }

}
