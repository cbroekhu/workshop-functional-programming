package com.cap.fp.completed.case_min1;

public class Screen2 {
	
	public static void main( String[] args){
		
		int offsetX = 20;
		final int offsetY = 40;
		
		Button button = new Button();
	
		button.addClickListener( 
				
				() -> System.out.println( "You clicked")
				
		);
		
		button.addMoveListener( 
				
				( x, y) -> System.out.println( "You moved to: " + ( offsetX + x) + "," + ( offsetY + y))
				
		);
		
		button.click();
		button.move( 542, 1265);
		
	}

}
