package com.cap.fp.completed.case_min1;

public class Button {
	
	ClickListener clickListener;
	MoveListener moveListener;

	public void addClickListener(ClickListener clickListener) {
		this.clickListener = clickListener;
	}


	public void addMoveListener(MoveListener moveListener) {
		this.moveListener = moveListener;
	}
	
	public void click(){
		clickListener.click();
	}

	public void move( int x, int y){
		moveListener.move( x, y);
	}

}
