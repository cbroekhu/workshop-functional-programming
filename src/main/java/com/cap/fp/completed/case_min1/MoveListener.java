package com.cap.fp.completed.case_min1;

public interface MoveListener {
	
	public void move(int x, int y);

}
