package com.cap.fp.completed.case0;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

class Dog{

    String name;
    int age;

    public Dog( String name, int age){
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return this.name;
    }
    public Integer getAge() {
        return this.age;
    }


}

public class Case0 {

    List<String> extractStringFromDogs(List<Dog> dogs, Function<Dog, String> f) {

        return dogs.stream().map(dog -> f.apply(dog)).collect(Collectors.toList());

    }

    List<Integer> extractIntegerFromDogs(List<Dog> dogs, Function<Dog, Integer> f) {

        return dogs.stream().map(dog -> f.apply(dog)).collect(Collectors.toList());

    }

    private void run() {

        List<Dog> dogs = new ArrayList();

        dogs.add( new Dog("Max", 8));
        dogs.add( new Dog("Fifi", 4));

        List<String> mappedDogsNames = extractStringFromDogs(dogs, dog -> dog.getName());

        mappedDogsNames.stream().forEach(

                System.out::println

        );

        List<Integer> mappedDogsAges = extractIntegerFromDogs(dogs, dog -> dog.getAge());

        mappedDogsAges.stream().forEach(

                System.out::println

        );

    }

    public static void main( String[] args){

        Case0 ft = new Case0();
        ft.run();

    }

}
