package com.cap.fp.case1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Case1 {

    private static final String STRINGTOFIND = "34567899999999876543";

    public static void main( String[] args) throws IOException {

        List<Path> paths = Files.list( Paths.get("D:/Data"))
 //               .filter( // // voeg geschikte lambda toe om te filteren op files en niet op folders)
 //               .filter( // voeg geschikte lambda toe om te filteren op extensie .txt)
                .collect( Collectors.toList());

        for( Path path: paths){

//            System.out.println( "File: " + path.getFileName());
            if( fileFound( path)){
                System.out.println( "File: " + path.getFileName());
                break;
            }

        }

    }

    private static boolean fileFound( Path path) throws IOException {

        boolean found = false;

        Optional<String> optional = Optional.of( Files.lines( path)
 //               .filter( // voeg geschikte lambda toe om de zoekstring te vinden)
                .peek(System.out::println)  //voor debuggen
                .findFirst()
                .orElse(""));

        if ( !optional.get().isEmpty()) {
            found = true;
        }

        return found;

    }

}
