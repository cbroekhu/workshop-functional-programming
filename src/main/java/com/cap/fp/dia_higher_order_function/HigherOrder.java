package com.cap.fp.dia_higher_order_function;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class HigherOrder {

    static Function<String, Integer> convertToWordsLength = String::length;
//    static Function<String, Integer> convertToWordLength2 = ( x) -> x.length();     // Hetzelfde

    static Function<String, Integer> convertToWordsLength2(){
        return  ( x) -> x.length();
    }

    static Function<Integer, String> getMyFunction() {
        return (it) ->  "Hello, world: " + it;
    }

    public static void main( String[] args){

        List<String> words = Arrays.asList("The", "That", "John", "Thanks");
//      List<Integer> numbers = Arrays.asList(8, 35, 1, 6);

//		words.stream()
//		.forEach( convertToWordCount);

        List<Integer> wordsCounts = words.stream().map(convertToWordsLength).collect(Collectors.toList());

        System.out.println( wordsCounts);

//        getMyFunction();


    }

}
