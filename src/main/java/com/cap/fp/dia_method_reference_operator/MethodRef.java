package com.cap.fp.dia_method_reference_operator;

import java.util.Arrays;
import java.util.List;

@FunctionalInterface
interface Printer {

    void print( String s);

}

public class MethodRef {

    private static void customPrinter( String x){

        System.out.println( x);

    }
    public static void main( String[] args){

        Printer printer = ( s) -> System.out.println( s);
        Printer printer2 =  System.out::println;
        Printer printer3 =  MethodRef::customPrinter;

        List<String> words = Arrays.asList("The", "That", "John", "Thanks");

        words.forEach( MethodRef::customPrinter);

    }

}
