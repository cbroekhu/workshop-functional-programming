package com.cap.fp.case_min3;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Case3 {

    public static void main(String args[]) {

        List<String> words = Arrays.asList("The", "That", "John", "Thanks");
        List<Integer> numbers = Arrays.asList(8, 35, 1, 6, 23);

//        Hieronder twee voorbeelden ter inspriratie om te oefenen met lambdas
//        Er zijn veel meer mogelijkheden. Maak er een aantal

        long i = words.stream()
                .filter( ( x) -> x.length() > 3)
                .count();


        List<Integer> ints = numbers.stream()
                .filter( ( x) -> x > 3)
                .distinct()
                .collect( Collectors.toList());




    }
}